FROM python:3

# set work directory
WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install dependencies
RUN pip install --upgrade pip
COPY ./requirements.txt /usr/src/app
RUN pip install -r requirements.txt

# copy project
COPY . /usr/src/app

RUN python manage.py test

# Set ENV after Run test
ENV DEBUG off
ENV SECRET_KEY your-secret-key
ENV DATABASE_URL psql://postgres:postgres@127.0.0.1:5432/wall
ENV CORS_ALLOWED_ORIGINS http://127.0.0.1:3000,http://localhost:3000
ENV ALLOWED_HOSTS *
ENV EMAIL_HOST smtp.adelantos.com.py
ENV EMAIL_PORT 587
ENV EMAIL_HOST_USER eratzlaff@adelantos.com.py
ENV EMAIL_HOST_PASSWORD 12345
ENV EMAIL_USE_TLS True

ENV DJANGO_SETTINGS_MODULE WallAppBackend.settings.prod

EXPOSE 8080

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
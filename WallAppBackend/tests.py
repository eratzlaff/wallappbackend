from django.contrib.auth.models import User
from django.urls import include, path, reverse
from rest_framework import status
from rest_framework.routers import DefaultRouter
from rest_framework.test import APITestCase, URLPatternsTestCase
from rest_framework_simplejwt.views import TokenRefreshView, TokenVerifyView
from WallAppBackend.jwt import MyTokenObtainPairView
from wall.views import WallViewSet


class JWTTests(APITestCase, URLPatternsTestCase):
    router = DefaultRouter()
    router.register(r'api/wall', WallViewSet)
    urlpatterns = [
        path('', include(router.urls)),
        path('api/token/', MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
        path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
        path('api/token/verify/', TokenVerifyView.as_view(), name='token_verify'),
    ]

    def test_tocken_jwt(self):
        # Auth
        User.objects.create_user(username='example', email='eratzlaff@adelantos.com.py', password='example')

        # Get token JWT
        url = reverse('token_obtain_pair')
        response = self.client.post(url, {'username': 'example', 'password': 'example'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertIsNotNone(response.data['access'])
        self.assertIsNotNone(response.data['refresh'])

        access = response.data['access']
        refresh = response.data['refresh']

        # Verify token
        url = reverse('token_verify')
        response = self.client.post(url, {'token': access})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Refresh token
        url = reverse('token_refresh')
        response = self.client.post(url, {'refresh': refresh})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertIsNotNone(response.data['access'])
        self.assertIsNotNone(response.data['refresh'])

        auth_headers = {
            'HTTP_AUTHORIZATION': 'Bearer ' + access,
        }

        # Check with auth
        url = reverse('wall-list')
        response = self.client.post(url, {'message': 'TEST', 'subject': 'TEST'}, format='json', **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(response.data), 6)
        self.assertEqual(response.data['message'], 'TEST')
        self.assertEqual(response.data['subject'], 'TEST')
        self.assertEqual(response.data['owner'], 'example')

        # Check not authenticated
        response = self.client.post(url, {'message': 'TEST'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

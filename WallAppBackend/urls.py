from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import TokenRefreshView, TokenVerifyView

from WallAppBackend.jwt import MyTokenObtainPairView
from wall.views import WallViewSet, UserViewSet

router = DefaultRouter()
router.register(r'api/wall', WallViewSet)
router.register(r'api/user', UserViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('api/token/', MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/token/verify/', TokenVerifyView.as_view(), name='token_verify'),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),

]

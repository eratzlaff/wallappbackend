# Wall App Backend

[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-black.svg)](https://sonarcloud.io/dashboard?id=eratzlaff_wallappbackend)

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=eratzlaff_wallappbackend&metric=alert_status)](https://sonarcloud.io/dashboard?id=eratzlaff_wallappbackend)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=eratzlaff_wallappbackend&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=eratzlaff_wallappbackend)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=eratzlaff_wallappbackend&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=eratzlaff_wallappbackend)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=eratzlaff_wallappbackend&metric=security_rating)](https://sonarcloud.io/dashboard?id=eratzlaff_wallappbackend)

[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=eratzlaff_wallappbackend&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=eratzlaff_wallappbackend)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=eratzlaff_wallappbackend&metric=bugs)](https://sonarcloud.io/dashboard?id=eratzlaff_wallappbackend)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=eratzlaff_wallappbackend&metric=code_smells)](https://sonarcloud.io/dashboard?id=eratzlaff_wallappbackend)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=eratzlaff_wallappbackend&metric=sqale_index)](https://sonarcloud.io/dashboard?id=eratzlaff_wallappbackend)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=eratzlaff_wallappbackend&metric=ncloc)](https://sonarcloud.io/dashboard?id=eratzlaff_wallappbackend)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=eratzlaff_wallappbackend&metric=coverage)](https://sonarcloud.io/dashboard?id=eratzlaff_wallappbackend)


## Getting Started

### Development Prerequisites

* Python 3.9
* [Wall App Frontend](https://bitbucket.org/eratzlaff/wallappfrontend/)

#### Install requirements
```shell
pip install -r requirements.txt
```
#### Run project in Development mode
```shell
DJANGO_SETTINGS_MODULE=WallAppBackend.settings.dev python manage.py runserver 0.0.0.0:8000
```

#### Run project in Production mode

Set the environment variables
````shell
cp ./WallAppBackend/settings/.env.example ./WallAppBackend/settings/.env
````
> Open and edit the `.env` file and change de Environment Variable to the production value

To run the backend in production mode
```shell
DJANGO_SETTINGS_MODULE=WallAppBackend.settings.prod python manage.py runserver 0.0.0.0:8000
```

### Container Prerequisites


In order to run this container you'll need docker installed.

* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)

### Usage

#### Container Parameters

Environment Variable
```dotenv
DEBUG=off
SECRET_KEY=your-secret-key
DATABASE_URL=psql://postgres:postgres@127.0.0.1:5432/wall
CORS_ALLOWED_ORIGINS=http://127.0.0.1:3000,http://localhost:3000
ALLOWED_HOSTS=*
EMAIL_HOST=smtp.adelantos.com.py
EMAIL_PORT=587
EMAIL_HOST_USER=eratzlaff@adelantos.com.py
EMAIL_HOST_PASSWORD=12345
EMAIL_USE_TLS=True
ENV DJANGO_SETTINGS_MODULE=WallAppBackend.settings.prod
```

Building the container

```shell
docker build --no-cache --rm --tag=wallappbackend .  
```

> Change the tag to a valid registry tag if you want to publish the container image on a registry

Run the continer `Example values`

```shell
docker run -p 8000:8000 \
-e DATABASE_URL=psql://postgres:postgres@192.168.0.2:5432/wall  \
-e EMAIL_HOST_PASSWORD=12345  \
...
wallappbackend
```

## Authors

* **Elvis Ratzlaff** - *Initial work* 


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
from django.db import models


class Wall(models.Model):
    created = models.DateTimeField(auto_now_add=True, blank=True)
    subject = models.CharField(max_length=100, null=False)
    message = models.TextField()
    owner = models.ForeignKey('auth.User', related_name='wall', on_delete=models.CASCADE)

from django.contrib.auth.models import User
from django.core import mail
from django.urls import include, path, reverse
from rest_framework import status
from rest_framework.routers import DefaultRouter
from rest_framework.test import APITestCase, URLPatternsTestCase
from WallAppBackend.jwt import MyTokenObtainPairView
from wall.views import WallViewSet, UserViewSet


class WallTests(APITestCase, URLPatternsTestCase):
    router = DefaultRouter()
    router.register(r'api/wall', WallViewSet)
    urlpatterns = [
        path('', include(router.urls)),
    ]

    def test_anonymous_wall(self):
        ## List walls
        url = reverse('wall-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)
        self.assertEqual(len(response.data['results']), 0)

        # Auth
        User.objects.create_user(username='example', email='eratzlaff@adelantos.com.py', password='example')
        self.client.login(username='example', password='example')

        # Create exampleo Wall
        url = reverse('wall-list')
        response = self.client.post(url, {'message': 'TEST', 'subject': 'TEST'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(response.data), 6)
        self.assertEqual(response.data['message'], 'TEST')
        self.assertEqual(response.data['owner'], 'example')
        self.client.logout()

        # Check create Wall
        url = reverse('wall-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)
        self.assertEqual(len(response.data['results']), 1)
        self.assertEqual(response.data['results'][0]['message'], 'TEST')
        self.assertEqual(response.data['results'][0]['owner'], 'example')
        self.assertEqual(response.data['results'][0]['id'], 1)

        # Get created Wall
        url = reverse('wall-detail', args=[1])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 6)
        self.assertEqual(response.data['id'], 1)
        self.assertEqual(response.data['message'], 'TEST')

        # Try Update Wall
        response = self.client.put(url, {'message': 'TEST2'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # Try Delete Wall
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # Get created Wall
        url = reverse('wall-detail', args=[1])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 6)
        self.assertEqual(response.data['id'], 1)
        self.assertEqual(response.data['message'], 'TEST')

    def test_auth_wall(self):
        url = reverse('wall-list')
        # Auth Wall
        User.objects.create_user(username='example', email='eratzlaff@adelantos.com.py', password='example')
        User.objects.create_user(username='example2', email='eratzlaff@adelantos.com.py', password='example')
        self.client.login(username='example', password='example')

        # Create Wall
        response = self.client.post(url, {'message': 'TEST', 'subject': 'TEST'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(response.data), 6)
        self.assertEqual(response.data['message'], 'TEST')
        self.assertEqual(response.data['owner'], 'example')
        self.client.logout()

        self.client.login(username='example2', password='example')

        # Create Wall other user
        response = self.client.post(url, {'message': 'TEST2', 'subject': 'TEST2'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(response.data), 6)
        self.assertEqual(response.data['message'], 'TEST2')
        self.assertEqual(response.data['subject'], 'TEST2')
        self.assertEqual(response.data['owner'], 'example2')
        self.client.logout()

        self.client.login(username='example', password='example')

        # List created Wall
        url = reverse('wall-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)
        self.assertEqual(len(response.data['results']), 2)

        # Check default order
        self.assertEqual(response.data['results'][0]['message'], 'TEST2')
        self.assertEqual(response.data['results'][0]['id'], 2)
        self.assertEqual(response.data['results'][0]['owner'], 'example2')

        self.assertEqual(response.data['results'][1]['message'], 'TEST')
        self.assertEqual(response.data['results'][1]['id'], 1)
        self.assertEqual(response.data['results'][1]['owner'], 'example')

        # Get created Wall
        url = reverse('wall-detail', args=[1])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 6)
        self.assertEqual(response.data['id'], 1)
        self.assertEqual(response.data['message'], 'TEST')

        # Update Wall
        url = reverse('wall-detail', args=[1])
        response = self.client.put(url, {'message': 'TEST2', 'subject': 'TEST2'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['message'], 'TEST2')
        self.assertEqual(response.data['subject'], 'TEST2')
        self.assertEqual(response.data['owner'], 'example')

        # Try Update other Wall
        url = reverse('wall-detail', args=[2])
        response = self.client.put(url, {'message': 'TEST2'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # Check Updated Wall
        url = reverse('wall-detail', args=[1])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['message'], 'TEST2')
        self.assertEqual(response.data['owner'], 'example')

        # Delete created Wall
        url = reverse('wall-detail', args=[1])
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # Try Delete other Wall
        url = reverse('wall-detail', args=[2])
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # Check deleted Wall
        url = reverse('wall-detail', args=[1])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class UserTests(APITestCase, URLPatternsTestCase):
    router = DefaultRouter()
    router.register(r'api/user', UserViewSet)
    urlpatterns = [
        path('', include(router.urls)),
        path('api/token/', MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    ]

    def test_user(self):
        url = reverse('user-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 0)

        user = {
            'username': 'eratzlaff',
            'email': 'eratzlaff@adelantos.com.py',
            'last_name': 'Ratzlaff',
            'first_name': 'Elvis',
            'password': 'Asdfg123$$'
        }

        # Register new User
        url = reverse('user-list')
        response = self.client.post(url, user, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['username'], 'eratzlaff')
        self.assertEqual(response.data['first_name'], 'Elvis')
        self.assertEqual(response.data['last_name'], 'Ratzlaff')
        self.assertEqual(response.data['email'], 'eratzlaff@adelantos.com.py')
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Welcome to Wall App Elvis Ratzlaff')
        self.assertEqual(mail.outbox[0].to, ['eratzlaff@adelantos.com.py'])

        user = {
            'username': 'eratzlaff2',
            'email': 'eratzlaff@adelantos.com.py',
            'last_name': 'Ratzlaff',
            'first_name': 'Elvis'
        }

        url = reverse('user-list')
        response = self.client.post(url, user, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # Get token JWT
        url = reverse('token_obtain_pair')
        response = self.client.post(url, {'username': 'eratzlaff', 'password': 'Asdfg123$$'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertIsNotNone(response.data['access'])
        self.assertIsNotNone(response.data['refresh'])

        access = response.data['access']

        auth_headers = {
            'HTTP_AUTHORIZATION': 'Bearer ' + access,
        }

        user = {
            'username': 'eratzlaff',
            'last_name': 'Ratzlaff Koop',
            'first_name': 'Elvis Hektor'
        }

        # Update user
        url = reverse('user-detail', args=[1])
        response = self.client.put(url, user, format='json', **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['username'], 'eratzlaff')
        self.assertEqual(response.data['first_name'], 'Elvis Hektor')
        self.assertEqual(response.data['last_name'], 'Ratzlaff Koop')
        self.assertEqual(response.data['email'], 'eratzlaff@adelantos.com.py')

        user = {
            'username': 'eratzlaff',
            'password': 'Pqwer123##'
        }

        # Update password
        url = reverse('user-detail', args=[1])
        response = self.client.put(url, user, format='json', **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['username'], 'eratzlaff')

        # Get token JWT
        url = reverse('token_obtain_pair')
        response = self.client.post(url, {'username': 'eratzlaff', 'password': 'Pqwer123##'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertIsNotNone(response.data['access'])
        self.assertIsNotNone(response.data['refresh'])

        access = response.data['access']

        auth_headers = {
            'HTTP_AUTHORIZATION': 'Bearer ' + access,
        }

        # Delete user
        url = reverse('user-detail', args=[1])
        response = self.client.delete(url, user, format='json', **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

from rest_framework import serializers
from django.contrib.auth import get_user_model
from django.contrib.auth.password_validation import validate_password

from wall.email import send_welcome_mail
from wall.models import Wall

UserModel = get_user_model()


class WallSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)
    message = serializers.CharField(required=True)
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = Wall
        fields = '__all__'


class UserSerializer(serializers.HyperlinkedModelSerializer):
    password = serializers.CharField(write_only=True, required=False)
    date_joined = serializers.DateTimeField(read_only=True)

    def validate_password(self, value):
        validate_password(value)
        return value

    def create(self, validated_data):
        user = get_user_model()(**validated_data)
        if validated_data.get('password'):
            user.set_password(validated_data['password'])
        else:
            raise serializers.ValidationError("password field required")

        ## Send Welcome email
        send_welcome_mail(user)
        user.save()
        return user

    def update(self, instance, validated_data):
        if validated_data.get('password'):
            instance.set_password(validated_data['password'])
        if validated_data.get('last_name'):
            instance.last_name = validated_data['last_name']
        if validated_data.get('first_name'):
            instance.first_name = validated_data['first_name']
        if validated_data.get('email'):
            instance.email = validated_data['email']
        instance.save()
        return instance

    class Meta:
        model = get_user_model()
        fields = ['url', 'id', 'last_name', 'first_name', 'username', 'email', 'password', 'date_joined']

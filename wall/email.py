from django.core.mail import send_mail


def send_welcome_mail(request):
    subject = "Welcome to Wall App " + request.first_name + " " + request.last_name

    body = {
        'first_name': request.first_name,
        'last_name': request.last_name,
        'username': request.username,
    }
    ## TODO: Create a welcome email
    message = "\n".join(body.values())

    send_mail(subject, message, 'eratzlaff@adelantos.com.py', [request.email])

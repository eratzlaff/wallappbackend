import django_filters
from django.contrib.auth.models import User
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, permissions
from rest_framework.filters import OrderingFilter

from wall.models import Wall
from wall.permissions import IsOwnerOrReadOnly, IsCurrentOwnerOrReadOnly
from wall.serializers import WallSerializer, UserSerializer


# Create your views here.


class WallViewSet(viewsets.ModelViewSet):
    queryset = Wall.objects.all()
    serializer_class = WallSerializer
    page_size = 5
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_fields = ['owner', 'message']
    search_fields = ['message']
    ordering = ['-created']
    ordering_fields = ['created']

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    permission_classes = [IsCurrentOwnerOrReadOnly]
    pagination_class = None
    page_size = None
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    search_backends = [DjangoFilterBackend]
    filter_fields = ['username', 'last_name', 'first_name']
    search_fields = ['^username', '^last_name', '^first_name']
    ordering = ['date_joined']
    ordering_fields = ['date_joined']
